#include <iostream>
#include <fstream>

#include "jsonscene.h"

using nlohmann::json;

void setDefault(json& object, const std::string& key, int value)
{
    if(object.count(key) == 0)
    {
        object[key] = value;
    }
}

void setDefault(json& object, const std::string& key, const std::string& value)
{
    if(object.count(key) == 0)
    {
        object[key] = value;
    }
}

void setDefault(json& object, const std::string& key, bool value)
{
    if(object.count(key) == 0)
    {
        object[key] = value;
    }
}

void setDefault(json& object, const std::string& key, float x, float y, float z)
{
    if(object.count(key) == 0)
    {
        object[key] = {x, y, z};
    }
}

void assertPresent(json& object, const std::string& key, const std::string& parent)
{
    if(object.count(key) == 0)
    {
        std::cerr << "Failed to load scene: value " << key << " in " << parent << " is missing" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void assertNumber(json& object, const std::string& key, const std::string& parent)
{
    if(!object[key].is_number())
    {
        std::cerr << "Failed to load scene: value " << key << " in " << parent << " must be number" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void assertBool(json& object, const std::string& key, const std::string& parent)
{
    if(!object[key].is_boolean())
    {
        std::cerr << "Failed to load scene: value " << key << " in " << parent << " must be boolean" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void assertArray(json& object, const std::string& key, const std::string& parent, int length = -1)
{
    if(!object[key].is_array())
    {
        std::cerr << "Failed to load scene: value " << key << " in " << parent << " must be array" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(length >= 0 && object[key].size() != (uint32_t)length)
    {
        std::cerr << "Failed to load scene: value " << key << " in " << parent << " must have length " << length << std::endl;
        exit(EXIT_FAILURE);
    }

    for(auto& item : object[key])
    {
        if(!item.is_number())
        {
            std::cerr << "Failed to load scene: value " << key << " in " << parent << " must be array of numbers" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

void assertString(json& object, const std::string& key, const std::string& parent)
{
    if(!object[key].is_string())
    {
        std::cerr << "Failed to load scene: value " << key << " in " << parent << " must be string" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void validateObject(json& object)
{
    if(!object.is_object())
    {
        std::cerr << "Failed to load scene: object entry must be an object" << std::endl;
        exit(EXIT_FAILURE);
    }

    assertPresent(object, "model", "object entry");
    setDefault(object, "texture", std::string("none.png"));
    setDefault(object, "script", std::string(""));
    setDefault(object, "position", 0, 0, 0);
    setDefault(object, "scale", 1, 1, 1);
    setDefault(object, "rotation", 0, 0, 0);
    setDefault(object, "instances", 1);
    setDefault(object, "firstInstance", 0);
    setDefault(object, "pipeline", 0);
    setDefault(object, "color", 1, 1, 1);

    for(auto it = object.begin(); it != object.end(); ++it)
    {
        if(it.key() == "texture" || it.key() == "model" || it.key() == "script")
        {
            assertString(object, it.key(), "object entry");
        }
        else if(it.key() == "scale" || it.key() == "rotation" || it.key() == "position" || it.key() == "color")
        {
            assertArray(object, it.key(), "object entry", 3);
        }
        else if(it.key() == "instances" || it.key() == "firstInstance" || it.key() == "pipeline")
        {
            assertNumber(object, it.key(), "object entry");
        }
        else
        {
            std::cerr << "Failed to load scene: unknown key in object entry: " << it.key() << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

void validateObjects(json& objects)
{
    if(!objects.is_array())
    {
        std::cerr << "Failed to load scene: objects must be an array" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(objects.empty())
    {
        std::cerr << "Failed to load scene: objects are empty: " << objects.size() << std::endl;
        exit(EXIT_FAILURE);
    }

    for(auto& object : objects)
    {
        validateObject(object);
    }
}

void validatePipeline(json& pipeline)
{
    if(!pipeline.is_object())
    {
        std::cerr << "Failed to load scene: pipelines entry must be an object" << std::endl;
        exit(EXIT_FAILURE);
    }
    setDefault(pipeline, "wireframe", false);
    assertPresent(pipeline, "vertexShader", "pipeline entry");
    assertPresent(pipeline, "fragmentShader", "pipeline entry");

    bool foundTesc = false;
    bool foundTese = false;

    for(auto it = pipeline.begin(); it != pipeline.end(); ++it)
    {
        if(it.key() == "wireframe")
        {
            assertBool(pipeline, it.key(), "pipeline entry");
        }
        else if(it.key() == "vertexShader" || it.key() == "fragmentShader")
        {
            assertString(pipeline, it.key(), "pipeline entry");
        }
        else if(it.key() == "tessellationControlShader")
        {
            foundTesc = true;
            assertString(pipeline, it.key(), "pipeline entry");
        }
        else if(it.key() == "tessellationEvaluationShader")
        {
            foundTese = true;
            assertString(pipeline, it.key(), "pipeline entry");
        }
        else if(it.key() == "patchControlPoints")
        {
            assertNumber(pipeline, it.key(), "pipeline entry");
        }
        else
        {
            std::cerr << "Failed to load scene: unknown key in pipeline entry object: " << it.key() << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    if(foundTesc != foundTese)
    {
        std::cerr << "Failed to load scene: tessellation control and tessellation evaluation shader must be specified both or not at all" << std::endl;
        exit(EXIT_FAILURE);
    }
    if(foundTesc)
    {
        assertPresent(pipeline, "patchControlPoints", "pipeline entry with enabled tessellation");
    }
    pipeline["useTessellation"] = foundTesc;
}

void validatePipelines(json& pipelines)
{
    if(!pipelines.is_array())
    {
        std::cerr << "Failed to load scene: pipelines must be an array" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(pipelines.empty())
    {
        std::cerr << "Failed to load scene: pipelines are empty" << std::endl;
        exit(EXIT_FAILURE);
    }

    for(auto& pipeline : pipelines)
    {
        validatePipeline(pipeline);
    }
}

void validateSurface(json& surface)
{
    if(!surface.is_object())
    {
        std::cerr << "Failed to load scene: surface must be an object" << std::endl;
        exit(EXIT_FAILURE);
    }
    setDefault(surface, "forceFifo", false);

    for(auto it = surface.begin(); it != surface.end(); ++it)
    {
        if(it.key() == "forceFifo")
        {
            assertBool(surface, it.key(), "surface");
        }
        else
        {
            std::cerr << "Failed to load scene: unknown key in surface object: " << it.key() << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

void validateLight(json& light)
{
    if(!light.is_object())
    {
        std::cerr << "Failed to load scene: lights must be an object" << std::endl;
        exit(EXIT_FAILURE);
    }

    assertPresent(light, "position", "light entry");
    for(auto it = light.begin(); it != light.end(); ++it)
    {
        if(it.key() == "position")
        {
            assertArray(light, it.key(), "light entry", 3);
        }
        else
        {
            std::cerr << "Failed to load scene: unknown key in light object: " << it.key() << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

void validateLights(json& lights)
{
    if(!lights.is_array())
    {
        std::cerr << "Failed to load scene: lights must be an array" << std::endl;
        exit(EXIT_FAILURE);
    }

    for(auto& light : lights)
    {
        validateLight(light);
    }
}

void validateCamera(json& camera)
{
    if(!camera.is_object())
    {
        std::cerr << "Failed to load scene: camera must be a json object" << std::endl;
        exit(EXIT_FAILURE);
    }
    setDefault(camera, "position", 0, 0, 3);
    setDefault(camera, "speed", 4);

    for(auto it = camera.begin(); it != camera.end(); ++it)
    {
        if(it.key() == "position")
        {
            assertArray(camera, it.key(), "camera");
        }
        else if(it.key() == "speed")
        {
            assertNumber(camera, it.key(), "camera");
        }
        else
        {
            std::cerr << "Failed to load scene: unknown key in camera object: " << it.key() << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

void validateSkybox(json& box)
{
    if(!box.is_object())
    {
        std::cerr << "Failed to load scene: skybox must be an object" << std::endl;
        exit(EXIT_FAILURE);
    }

    assertPresent(box, "left", "skybox");
    assertPresent(box, "right", "skybox");
    assertPresent(box, "top", "skybox");
    assertPresent(box, "bottom", "skybox");
    assertPresent(box, "front", "skybox");
    assertPresent(box, "back", "skybox");
    assertPresent(box, "pipeline", "skybox");

    for(auto it = box.begin(); it != box.end(); ++it)
    {
        if(it.key() == "left" || it.key() == "right" || it.key() == "top" || it.key() == "bottom" || it.key() == "front" || it.key() == "back")
        {
            assertString(box, it.key(), "skybox");
        }
        else if(it.key() == "pipeline")
        {
            assertNumber(box, it.key(), "skybox");
        }
        else
        {
            std::cerr << "Failed to load scene: unknown key in skybox object: " << it.key() << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

json loadScene(const std::string& filename)
{
    std::string path = "scenes/" + filename + ".json";
    std::ifstream fin(path, std::ios::ate | std::ios::binary);
    if(!fin.is_open())
    {
        path = "../" + path;
        fin.open(path);
        if(!fin.is_open())
        {
            std::cerr << "Could not open scene JSON" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    json scene;
    fin >> scene;

    if(!scene.is_object())
    {
        std::cerr << "Could not open scene: scene is not a json object" << std::endl;
        exit(EXIT_FAILURE);
    }

    if(scene.count("surface") == 0)
    {
        scene["surface"] = json::object();
    }
    if(scene.count("objects") == 0)
    {
        scene["objects"] = json::array();
    }
    if(scene.count("pipelines") == 0)
    {
        scene["pipelines"] = json::array();
    }
    if(scene.count("lights") == 0)
    {
        scene["lights"] = json::array();
    }
    if(scene.count("camera") == 0)
    {
        scene["camera"] = json::object();
    }
    scene["useSkybox"] = false;

    for(auto it = scene.begin(); it != scene.end(); ++it)
    {
        if(it.key() == "objects")
        {
            validateObjects(it.value());
        }
        else if(it.key()  == "pipelines")
        {
            validatePipelines(it.value());
        }
        else if(it.key()  == "surface")
        {
            validateSurface(it.value());
        }
        else if(it.key()  == "lights")
        {
            validateLights(it.value());
        }
        else if(it.key() == "camera")
        {
            validateCamera(it.value());
        }
        else if(it.key() == "skybox")
        {
            validateSkybox(it.value());
            scene["useSkybox"] = true;
        }
        else if(it.key() == "useSkybox")
        {
            // pass
        }
        else
        {
            std::cerr << "Could not open scene: found unknown key: " << it.key() << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    return scene;
}
#include <glm/gtx/euler_angles.hpp>

#include "object.h"
#include "util.h"

Object::Object(const Model& model, const Shader& shader, const nlohmann::json& config)
    : model(model), shader(shader),
      position(jsonToVec3(config["position"])),
      scale(jsonToVec3(config["scale"])),
      rotation(jsonToVec3(config["rotation"])),
      color(jsonToVec3(config["color"])),
      instances(config["instances"].get<int>()),
      firstInstance(config["firstInstance"].get<int>())
{

}

void Object::draw(const glm::mat4& projection, const glm::mat4& view) const
{
    shader.use();
    shader.set("projection", projection);
    shader.set("view", view);
    glm::mat4 matrix = glm::translate(glm::mat4(1.0f), position);
    matrix *= glm::eulerAngleXYZ(rotation.x, rotation.y, rotation.z);
    matrix = glm::scale(matrix, scale);
    shader.set("model", matrix);
    shader.set("color", color);
    shader.set("instances", instances);
    shader.set("firstInstance", firstInstance);

    if(instances > 1)
    {
        model.draw(instances);
    }
    else
    {
        model.draw();
    }

}

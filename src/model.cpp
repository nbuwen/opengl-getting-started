#include <unordered_map>

#include <tiny_obj_loader.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "model.h"

void Model::draw() const
{
    for (auto &mesh : meshes)
    {
        mesh.draw();
    }
}

void Model::draw(int instances) const
{
	for (auto &mesh : meshes)
    {
        mesh.draw(instances);
    }
}

Model::Model(const std::string &path, const std::string &tpath)
{
        loadModel(path, tpath);
}

void Model::loadModel(const std::string& modelFile, const std::string& textureFile) {
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;

    std::string error;
    std::string path = "../models/" + modelFile;
    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &error, path.c_str());

    if (!error.empty())
    {
        std::cerr << error << std::endl;
    }

    if (!ret)
    {
        exit(1);
    }

    std::unordered_map<Vertex, uint32_t, Vertex::Hasher> unique;
    std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;
    std::vector<Texture> textures;

    for(const auto& shape : shapes)
    {
        for(const auto& index : shape.mesh.indices)
        {
            Vertex vertex;

            vertex.Position = {
                attrib.vertices[3 * index.vertex_index + 0],
                attrib.vertices[3 * index.vertex_index + 1],
                attrib.vertices[3 * index.vertex_index + 2],
            };

            vertex.Normal = {
                attrib.normals[3 * index.normal_index + 0],
                attrib.normals[3 * index.normal_index + 1],
                attrib.normals[3 * index.normal_index + 2],
            };

            glm::vec2 texCoord;
            if(!attrib.texcoords.empty())
            {
				vertex.TexCoords = {
					attrib.texcoords[2 * index.texcoord_index + 0],
                    attrib.texcoords[2 * index.texcoord_index + 1]
                };
            }

            if(unique.count(vertex) == 0)
            {
                unique[vertex] = (uint32_t)vertices.size();
                vertices.push_back(vertex);
            }
            indices.push_back(unique[vertex]);
        }
    }

	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	stbi_set_flip_vertically_on_load(true);
    path = "../textures/" + textureFile;
	unsigned char *data = stbi_load(path.c_str(), &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
        switch(nrComponents)
        {
        case 1:
            format = GL_RED;
            break;
        case 3:
            format = GL_RGB;
            break;
        case 4:
            format = GL_RGBA;
            break;
        default:
            error = "Failed to load texture '" + path + "': cannot handle " + std::to_string(nrComponents) + " channels";
            throw std::runtime_error(error);
        }


		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}
	else
	{
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free(data);
	}

	Texture texture;
	texture.id = textureID;
	texture.type = "texture_diffuse";
	textures.push_back(texture);

    meshes.emplace_back(vertices, indices, textures);
}


#ifndef OPENGLGETTINGSTARTED_MESH_H
#define OPENGLGETTINGSTARTED_MESH_H

#include <string>
#include <vector>

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "shader.h"


struct Vertex
{
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TexCoords;

    bool operator==(const Vertex& vertex) const;

    struct Hasher
    {
        std::size_t operator()(const Vertex& vertex) const;
    };
};

struct Texture
{
    unsigned int id;
    std::string type;
};

class Mesh
{
public:


    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures = {});
    void draw() const;
	void draw(int instances) const;
private:
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;

    unsigned int VAO, VBO, EBO;

    void beforeDraw() const;
    void afterDraw() const;

    void setupMesh();
};


#endif //OPENGLGETTINGSTARTED_MESH_H

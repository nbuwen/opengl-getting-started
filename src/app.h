#ifndef APP_H
#define APP_H

#include <unordered_map>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <json.hpp>

#include "shader.h"
#include "camera.h"
#include "shader.h"
#include "model.h"
#include "object.h"


class App
{
public:
	App(int width, int height, nlohmann::json scene);
	bool init();
	bool shouldRun();
	void loop();

private:
    const Model& createModel(const std::string& model, const std::string& texture);
	static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
	void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods);
    void mouseCallback(GLFWwindow* window, double xpos, double ypos);

	struct
	{
		GLFWwindow* handle = nullptr;
		int width = 0;
		int height = 0;
	} window;

    float admix;

	struct
	{
		float delta = 0.0f;
		float lastFrame = 0.0f;
        float fpsTime = 0.0f;
        int fpsCount = 0;
	} time;

	struct
	{
		glm::vec2 lastPosition = {800, 600};
		bool firstCallback = true;
	} mouse;

    struct
    {
        nlohmann::json config;
        std::vector<Object> objects = {};
        std::vector<Shader> shaders = {};
        std::unordered_map<std::string, Model> models = {};
    } scene;

    struct
    {
        bool up = false;
        bool down = false;
        bool left = false;
        bool right = false;
        bool forward = false;
        bool backward = false;
    } input;

    Camera* camera = nullptr;
};

#endif // !APP_H


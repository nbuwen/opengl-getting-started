#ifndef OPENGLGETTINGSTARTED_OBJECT_H
#define OPENGLGETTINGSTARTED_OBJECT_H

#include <glm/glm.hpp>
#include <json.hpp>

#include "model.h"
#include "shader.h"

class Object
{
    const Model& model;
    const Shader& shader;

    glm::vec3 position;
    glm::vec3 scale;
    glm::vec3 rotation;
    glm::vec3 color;
    int instances;
    int firstInstance;
public:
    Object(const Model& model, const Shader& shader, const nlohmann::json& config);
    void draw(const glm::mat4& projection, const glm::mat4& view) const;
};


#endif //OPENGLGETTINGSTARTED_OBJECT_H

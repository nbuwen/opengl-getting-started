#include "util.h"

glm::vec3 jsonToVec3(nlohmann::json array)
{
    return {array[0].get<float>(), array[1].get<float>(), array[2].get<float>()};
}
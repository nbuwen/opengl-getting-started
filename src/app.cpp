#include <iostream>
#include <cmath>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "app.h"
#include "util.h"

App::App(int width, int height, nlohmann::json scene)
{
    window.width = width;
    window.height = height;
    this->scene.config = std::move(scene);
}

bool App::init()
{
	admix = 0.5f;

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window.handle = glfwCreateWindow(window.width, window.height, "LearnOpenGL", nullptr, nullptr);
	if (window.handle == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return false;
	}


	glfwMakeContextCurrent(window.handle);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return false;
	}

	
    glfwSetWindowUserPointer(window.handle, this);

    auto func1 = [](GLFWwindow* w, int a, int b, int c, int d)
    {
        static_cast<App*>(glfwGetWindowUserPointer(w))->keyCallback(w,a,b,c,d);
    };

    auto func2 = [](GLFWwindow* w, double xpos, double ypos)
    {
        static_cast<App*>(glfwGetWindowUserPointer(w))->mouseCallback(w, xpos, ypos);
    };

	glfwSetFramebufferSizeCallback(window.handle, framebuffer_size_callback);
	glfwSetKeyCallback(window.handle, func1);

    glfwSetInputMode(window.handle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPosCallback(window.handle, func2);

	glEnable(GL_DEPTH_TEST);

    if(!scene.config["surface"]["forceFifo"].get<bool>())
    {
        glfwSwapInterval(0);
    }

    for(const auto& pipeline : scene.config["pipelines"])
    {
        const std::string& vert = pipeline["vertexShader"].get<std::string>();
        const std::string frag = pipeline["fragmentShader"].get<std::string>();

        if(pipeline["useTessellation"].get<bool>())
        {
            const std::string& tesc = pipeline["tessellationControlShader"].get<std::string>();
            const std::string tese = pipeline["tessellationEvaluationShader"].get<std::string>();
            scene.shaders.emplace_back(vert, frag, tesc, tese);
        }
        else
        {
            scene.shaders.emplace_back(vert, frag);
        }
    }

    for(const auto& object : scene.config["objects"])
    {
        const std::string& path = object["model"].get<std::string>();
        auto shaderIndex = object["pipeline"].get<int>();

        if(shaderIndex < 0 || shaderIndex >= static_cast<int>(scene.shaders.size()))
        {
            std::cerr << "Error: Failed to load object: invalid pipeline index" << std::endl;
            continue;
        }

        const Shader& shader = scene.shaders[shaderIndex];
        const Model& model = createModel(object["model"].get<std::string>(), object["texture"].get<std::string>());
        scene.objects.emplace_back(model, shader, object);
    }

    camera = new Camera(jsonToVec3(scene.config["camera"]["position"]));


	
	return true;
}

bool App::shouldRun()
{
	return !glfwWindowShouldClose(window.handle);
}

void App::loop()
{
    auto currentFrame = static_cast<float>(glfwGetTime());
    time.delta = currentFrame - time.lastFrame;
    time.lastFrame = currentFrame;
    time.fpsTime += time.delta;
    ++time.fpsCount;
    if(time.fpsTime >= 1.0f)
    {
        std::cout << "FrameTime: " << time.fpsTime / time.fpsCount << " - FPS    " << time.fpsCount << std::endl;
        time.fpsTime -= 1.0f;
        time.fpsCount = 0;
    }

    if(input.forward) camera->ProcessKeyboard(CameraMovement::FORWARD, time.delta);
    if(input.backward) camera->ProcessKeyboard(CameraMovement::BACKWARD, time.delta);
    if(input.left) camera->ProcessKeyboard(CameraMovement::LEFT, time.delta);
    if(input.right) camera->ProcessKeyboard(CameraMovement::RIGHT, time.delta);
    if(input.up) camera->ProcessKeyboard(CameraMovement::UP, time.delta);
    if(input.down) camera->ProcessKeyboard(CameraMovement::DOWN, time.delta);

    // rendering commands
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 projection = glm::perspective(glm::radians(60.0f), window.width / static_cast<float>(window.height), 0.1f, 10000.0f);
    glm::mat4 view = camera->GetViewMatrix();


    for(const auto& object : scene.objects)
    {
        object.draw(projection, view);
    }

	glfwSwapBuffers(window.handle);
	glfwPollEvents();
}

void App::framebuffer_size_callback(GLFWwindow*, int width, int height)
{
	glViewport(0, 0, width, height);
}


void App::keyCallback(GLFWwindow *window, int key, int, int action, int)
{
	switch (key)
    {
	case GLFW_KEY_ESCAPE:
		if (action == GLFW_PRESS) glfwSetWindowShouldClose(window, true);
		break;
	case GLFW_KEY_L:
        glPolygonMode(GL_FRONT_AND_BACK, (action != GLFW_RELEASE)? GL_LINE : GL_FILL);
		break;
    case GLFW_KEY_W:
        input.forward = action != GLFW_RELEASE;
        break;
    case GLFW_KEY_S:
        input.backward = action != GLFW_RELEASE;
        break;
    case GLFW_KEY_A:
        input.left = action != GLFW_RELEASE;
        break;
    case GLFW_KEY_D:
        input.right = action != GLFW_RELEASE;
        break;
    case GLFW_KEY_LEFT_SHIFT:
        input.up = action != GLFW_RELEASE;
        break;
    case GLFW_KEY_LEFT_CONTROL:
        input.down = action != GLFW_RELEASE;
        break;
	default:
		break;
	}
}

void App::mouseCallback(GLFWwindow*, double x, double y)
{
    glm::vec2 position = {x, y};

    if(mouse.firstCallback) // this bool variable is initially set to true
    {
        mouse.lastPosition = position;
        mouse.firstCallback = false;
    }

    auto offset = position - mouse.lastPosition;
    offset.y *= -1;
    mouse.lastPosition = position;

    camera->ProcessMouseMovement(offset.x, offset.y);
}

const Model &App::createModel(const std::string &model, const std::string &texture)
{
    if(scene.models.count(model) == 0)
    {
        scene.models.emplace(
                std::piecewise_construct,
                std::forward_as_tuple(model),
                std::forward_as_tuple(model, texture)
        );
    }

    return scene.models.at(model);
}





#ifndef OPENGLGETTINGSTARTED_UTIL_H
#define OPENGLGETTINGSTARTED_UTIL_H

#include <glm/glm.hpp>
#include <json.hpp>

glm::vec3 jsonToVec3(nlohmann::json array);


#endif //OPENGLGETTINGSTARTED_UTIL_H

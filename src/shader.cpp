#include "shader.h"


std::string shaderTypeToExt(GLuint shaderType)
{
    switch(shaderType)
    {
    case GL_VERTEX_SHADER:
        return ".vert";
    case GL_FRAGMENT_SHADER:
        return ".frag";
    case GL_TESS_CONTROL_SHADER:
        return ".tesc";
    case GL_TESS_EVALUATION_SHADER:
        return ".tese";
    default:
        return ".UNKNOWN";
    }
}


GLuint createShader(const std::string& filename, GLuint shaderType)
{
    std::string code;
    std::ifstream fin;

    fin.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    try
    {
        std::string path = "../shaders/" + filename + shaderTypeToExt(shaderType);
        fin.open(path);
        std::stringstream stream;
        stream << fin.rdbuf();
        fin.close();
        code = stream.str();
    }
    catch (const std::exception& e)
    {
        std::cerr << "ERROR::SHADER::FILE_NOT_SUCCESSFULLY_READ " << e.what() << std::endl;
    }

    GLuint handle;
    int success;

    handle = glCreateShader(shaderType);
    const char* charCode = code.c_str();
    glShaderSource(handle, 1, &charCode, nullptr);
    glCompileShader(handle);

    glGetShaderiv(handle, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        char infoLog[512];
        glGetShaderInfoLog(handle, 512, nullptr, infoLog);
        std::cerr << "ERROR:SHADER::COMPILATION_FAILED" << std::endl << infoLog << std::endl;
    }

    return handle;
}


GLuint createProgram(GLuint vertex, GLuint fragment, GLuint tesc=0, GLuint tese=0)
{
    GLuint handle = glCreateProgram();
    glAttachShader(handle, vertex);
    glAttachShader(handle, fragment);
    if (tesc != 0)
    {
        glAttachShader(handle, tesc);
        glAttachShader(handle, tese);
    }
    glLinkProgram(handle);

    int success;
    glGetProgramiv(handle, GL_LINK_STATUS, &success);
    if (!success)
    {
        char infoLog[512];
        glGetProgramInfoLog(handle, 512, nullptr, infoLog);
        std::cerr << "ERROR::SHADER::PROGRAM::LINKING_FAILED" << std::endl << infoLog << std::endl;
    }

    glDeleteShader(vertex);
    glDeleteShader(fragment);
    if (tesc != 0)
    {
        glDeleteShader(tesc);
        glDeleteShader(tese);
    }
    return handle;
}


Shader::Shader(const std::string& vertex, const std::string& fragment, const std::string& tesc, const std::string& tese)
{
    id = createProgram(
            createShader(vertex, GL_VERTEX_SHADER),
            createShader(fragment, GL_FRAGMENT_SHADER),
            createShader(tesc, GL_TESS_CONTROL_SHADER),
            createShader(tese, GL_TESS_EVALUATION_SHADER)
    );
}

Shader::Shader(const std::string& vertex, const std::string& fragment)
{
    id = createProgram(
            createShader(vertex, GL_VERTEX_SHADER),
            createShader(fragment, GL_FRAGMENT_SHADER)
    );
}


void Shader::use() const
{
    glUseProgram(id);
}

void Shader::setBool(const std::string &name, bool value) const
{
    glUniform1i(glGetUniformLocation(id, name.c_str()), (int)value);
}

void Shader::setFloat(const std::string &name, float value) const
{
    glUniform1f(glGetUniformLocation(id, name.c_str()), value);
}

void Shader::set(const std::string &name, const glm::mat4 &mat) const
{
    glUniformMatrix4fv(glGetUniformLocation(id, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::set(const std::string &name, const glm::vec3 &vec) const
{
    glUniform3fv(glGetUniformLocation(id, name.c_str()), 1, &vec[0]);
}

void Shader::set(const std::string &name, int value) const
{
    glUniform1i(glGetUniformLocation(id, name.c_str()), value);
}

#version 400 core
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 inColor;

void main()
{
	gl_FragColor = vec4(inColor, 1.0);
}

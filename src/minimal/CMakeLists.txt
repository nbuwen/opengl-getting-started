cmake_minimum_required(VERSION 3.0)
project(OpenglGettingStarted-Minimal)

if(MSVC)
    # Force to always compile with W4
    if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
        string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
    endif()
elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
    # Update if necessary
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wno-long-long -pedantic")
endif()

set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
set(GLAD_PROFILE core CACHE STRING "" FORCE)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

if(TARGET glfw)
else()
    add_subdirectory(external/glfw-3.2.1)
endif()

if(TARGET glad)
else()
    file(GLOB GLAD_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/external/glad/*.*)
    add_library(glad ${GLAD_SOURCES})
endif()

find_package(OpenGL REQUIRED)

if(TARGET base)
    add_executable(minimal EXCLUDE_FROM_ALL minimal.cpp)
else()
    add_executable(minimal minimal.cpp)
endif()

target_link_libraries(minimal glfw)
target_link_libraries(minimal glad)
target_include_directories(minimal PUBLIC ${OPENGL_INCLUDE_DIR})
target_include_directories(minimal PUBLIC external)
target_link_libraries(minimal ${OPENGL_gl_LIBRARY})

file(GLOB FRAGMENT_SHADERS ${CMAKE_CURRENT_SOURCE_DIR}/*.frag)
file(GLOB VERTEX_SHADERS ${CMAKE_CURRENT_SOURCE_DIR}/*.vert)

add_custom_command(TARGET minimal POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy ${VERTEX_SHADERS} ${CMAKE_CURRENT_BINARY_DIR})
add_custom_command(TARGET minimal POST_BUILD COMMAND ${CMAKE_COMMAND} -E copy ${FRAGMENT_SHADERS} ${CMAKE_CURRENT_BINARY_DIR})

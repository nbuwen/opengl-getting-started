Minimal Getting Started Program
===============================

GNU/Linux Dependencies
----------------------

- `xorg-dev`

Setup
-----

- Install [CMake](https://cmake.org/)
- Have a working C++ compiler with C++14 support
- Update GPU drivers
- Download and extract [glfw-3.2.1](http://www.glfw.org/) into `external`


Build
-----

- `mkdir build` and `cd build`
- `cmake ..`
- Build with your toolchain


Run
---

- On GNU/Linux type `./minimal`
- On Window type `minimal.exe`

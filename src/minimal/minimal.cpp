#include <iostream>
#include <fstream>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

// used later for glClearColor
#define DARK_GREY 0.2f, 0.2f, 0.2f, 1.0f

/**
 * Hold the basic information needed for the render loop
 *
 * programId:       the opengl id of the linked shader program
 * vertexArrayId:   the opengl id of the vertex array object
 */
struct Scene
{
    GLuint programId = 0;
    GLuint vertexArrayId = 0;
};

/**
 * Describe the format of a single vertex
 *
 * position:    a position consists of a x, y and z coordinates
 * color:       the color consists of r, g and b channels
 */
struct Vertex
{
    float position[3];
    float color[3];
};

// The vertex data for a rectangle in the middle of the screen
// the first column is the position
// the second column is the color
std::vector<Vertex> vertices = {
        {{-0.5f, -0.5f, 0}, {1, 0, 0}},
        {{0.5f, -0.5f, 0}, {0.5f, 1, 0}},
        {{0.5f, 0.5f, 0}, {0, 1, 1}},
        {{-0.5f, 0.5f, 0}, {0.5f, 0, 1}},
};

// The element data for the glDrawElements call
// every row describes a triangle
std::vector<GLuint> elements = {
        0, 1, 2,
        0, 2, 3,
};

GLFWwindow* createWindow(int width, int height);
void onWindowResize(GLFWwindow *, int width, int height);
void onKeyPress(GLFWwindow* window, int key, int, int action, int);

void initGlad();
void initGL(Scene& scene);

void drawScene(const Scene& scene);
void cleanup(GLFWwindow* window);

void updateUniforms(const Scene& scene);

GLuint createShader(const std::string& filename, GLuint shaderType);
GLuint createShaderProgram(GLuint vertexShaderId, GLuint fragmentShaderId);



int main()
{
    GLFWwindow* window = nullptr;
    Scene scene;
    try
    {
        window = createWindow(1920, 1080);

        initGlad();
        initGL(scene);
    }
    catch(const std::runtime_error& e)
    {
        std::cerr << e.what() << std::endl;
        cleanup(window);
        exit(EXIT_FAILURE);
    }

    // increment frameCount and add the frame time every frame until
    //   it reaches one second.
    // After that set both to zero and print frameCount/time
    double time = 0;
    int frameCount = 0;

    // loop until the window is closed
    while(!glfwWindowShouldClose(window))
    {
        double startTime = glfwGetTime();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        updateUniforms(scene);
        drawScene(scene);

        // use the next buffer in the swapchain for the next frame
        glfwSwapBuffers(window);
        // check if keys where pressed, the window was closed etc.
        glfwPollEvents();
        time += glfwGetTime() - startTime;
        ++frameCount;
        if(time >= 1)
        {
            std::cout << "FrameTime: " << time / frameCount;
            std::cout << " - FPS: " << frameCount << std::endl;
            frameCount = 0;
            time = 0;
        }
    }

    cleanup(window);
}

/**
 * Update the viewport when the window is resized
 *
 * @param width the new width of the window
 * @param height the new height of the window
 */
void onWindowResize(GLFWwindow *, int width, int height)
{
    glViewport(0, 0, width, height);
}

/**
 * Handle key presses/releases
 *
 * Currently only close the window when ESC was pressed
 *
 * @param window the window that captured the key press/release
 * @param key the key that was pressed/released
 */
void onKeyPress(GLFWwindow* window, int key, int, int /*action*/, int)
{
    switch(key)
    {
    case GLFW_KEY_ESCAPE:
        glfwSetWindowShouldClose(window, true);
    default:
        break;
    }
}

/**
 * Create a GLFW window with given dimensions
 *
 * @param width initial width of the window
 * @param height initial height of the window
 * @return a pointer to the window. The caller takes ownership of the pointer
 */
GLFWwindow* createWindow(int width, int height)
{
    glfwInit();

    // use OpenGL core 4.5
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // first nullptr means windowed mode instead of fullscreen
    // second nullptr is not important
    GLFWwindow* window = glfwCreateWindow(width, height, "Minimal OpenGL", nullptr, nullptr);

    if(window == nullptr)
    {
        throw std::runtime_error("Failed to create GLFW window");
    }

    glfwSetKeyCallback(window, onKeyPress);

    // use the specified opengl version for this window
    glfwMakeContextCurrent(window);
    // set the "on window resize" callback
    glfwSetFramebufferSizeCallback(window, onWindowResize);

    // disable vsync
    glfwSwapInterval(0);

    return window;
}

/**
 * Load the glad loader
 *
 * This is necessary because the default opengl headers often export old opengl functions.
 * E.g. on windows only OpenGL 1.3 is available
 */
void initGlad()
{
    if(!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)))
    {
        throw std::runtime_error("Failed to initialize GLAD");
    }
}

/**
 * Create the scene that will be rendered later in the main loop
 *
 * @param scene (out parameter) the function will fill the scene with all necessary information
 */
void initGL(Scene& scene)
{
    // enable the z-buffer
    glEnable(GL_DEPTH_TEST);
    // use dark grey for the background color
    glClearColor(DARK_GREY);

    // the opengl pipeline always needs a vertex and a fragment shader
    GLuint vertexShaderId = createShader("minimal.vert", GL_VERTEX_SHADER);
    GLuint fragmentShaderId = createShader("minimal.frag", GL_FRAGMENT_SHADER);

    GLuint programId = createShaderProgram(vertexShaderId, fragmentShaderId);

    // the vertex array object holds buffer and image data
    GLuint vertexArrayId;
    // the vertex buffer object holds the vertex positions and colors
    GLuint vertexBufferId;
    // the element buffer object holds the index data to reduce vertex duplication
    GLuint elementBufferId;

    glGenVertexArrays(1, &vertexArrayId);
    glGenBuffers(1, &vertexBufferId);
    glGenBuffers(1, &elementBufferId);

    // use the vertex array object from now on
    glBindVertexArray(vertexArrayId);

    // attach the vertex buffer to the vertex array and fill it with vertex data
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
    // arguments are: buffer type, size of the vertex data, pointer to the vertex data, memory hint (data won't change)
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);

    // attach the element buffer to the vertex array and fill it with index data
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferId);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.size() * sizeof(GLuint), elements.data(), GL_STATIC_DRAW);

    // describe the format of the vertex buffer

    // first part is the vertex position
    // enable it
    glEnableVertexAttribArray(0);
    // describe it
    glVertexAttribPointer(0, // the previously enabled attrib position
                          3, // number of elements (x,y,z)
                          GL_FLOAT, // type of the elements
                          GL_FALSE, // don't normalize (only useful for vertex normals)
                          sizeof(Vertex), // memory stride i.e. the next position is sizeof(Vertex) bytes farther
                          reinterpret_cast<void*>(offsetof(Vertex, position))); // the offset is the same as the offset of in the Vertex struct

    // second part is the vertex color
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, color)));

    // unbind the vertex array object to prevent accidental modification
    glBindVertexArray(0);

    scene.programId = programId;
    scene.vertexArrayId = vertexArrayId;
}

/**
 * Create a shader from source
 *
 * @param filename the file name that contains the shader code
 * @param shaderType the shader type (vertex shader, fragment shader)
 * @return an id that uniquely identifies the shader
 */
GLuint createShader(const std::string& filename, GLuint shaderType)
{
    std::ifstream fin(filename);

    if(!fin.good())
    {
        throw std::runtime_error("Failed to open file " + filename);
    }

    // c++ "idiom" to read a file into a string
    std::string code((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());

    GLuint shaderId = glCreateShader(shaderType);

    // glShaderSource needs a char* argument
    const char* codeAsCharArray = code.c_str();

    // set the shader source and compile it
    // arguments are:   the target shader
    //                  number of code "lines" (we put everything into one "line")
    //                  address of the "first" line
    //                  number of lines or nullptr for a single line
    glShaderSource(shaderId, 1, &codeAsCharArray, nullptr);
    glCompileShader(shaderId);

    // error handling below
    int success;
    // check for successful compilation
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);

    if(!success)
    {
        char infoLog[512];
        glGetShaderInfoLog(shaderId, 512, nullptr, infoLog);
        std::string shaderTypeName = shaderType == GL_VERTEX_SHADER? "vertex":"fragment";
        std::cerr << infoLog << std::endl;
        throw std::runtime_error("Failed to compile " + shaderTypeName + " shader");
    }

    return shaderId;
}

/**
 * Create a shader progrm from a vertex and a fragment shader
 *
 * @param vertexShaderId the id of the compiled vertex shader to link
 * @param fragmentShaderId the id of the compiled fragment shader to link
 * @return an id that uniquely identifies the shader program
 */
GLuint createShaderProgram(GLuint vertexShaderId, GLuint fragmentShaderId)
{
    GLuint programId = glCreateProgram();
    glAttachShader(programId, vertexShaderId);
    glAttachShader(programId, fragmentShaderId);
    glLinkProgram(programId);

    // error handling below
    int success;
    glGetProgramiv(programId, GL_LINK_STATUS, &success);

    if(!success)
    {
        char infoLog[512];
        glGetProgramInfoLog(programId, 512, nullptr, infoLog);
        std::cerr << infoLog << std::endl;
        throw std::runtime_error("Failed to link shader program");
    }

    // after linking the shaders are no longer needed
    glDeleteShader(vertexShaderId);
    glDeleteShader(fragmentShaderId);

    return programId;
}

/**
 * Update the uniform values for the shaders (i.e. upload the data to the gpu)
 *
 * @param scene contains the target shader for the uniform data
 */
void updateUniforms(const Scene& scene)
{
    float identity[16] = {
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
    };

    // set the uniform matrix with name "model" to the identity matrix
    // arguments are:   position in the shader (determined from name), number of values (e.g. array of matrices)
    //                  don't transpose the matrix, pointer to the data
    glUniformMatrix4fv(glGetUniformLocation(scene.programId, "model"), 1, GL_FALSE, identity);
    glUniformMatrix4fv(glGetUniformLocation(scene.programId, "view"), 1, GL_FALSE, identity);
    glUniformMatrix4fv(glGetUniformLocation(scene.programId, "projection"), 1, GL_FALSE, identity);
}

void drawScene(const Scene& scene)
{
    // use the array object from above so we can draw the data in the vertex buffer
    glBindVertexArray(scene.vertexArrayId);
    // use the program linked above
    glUseProgram(scene.programId);

    // draw the vertex data using the elements buffer
    // arguments are:   polygon mode (we have 3 indices per primitive)
    //                  number of elements to draw
    //                  type of the indices
    //                  offset in the element buffer
    glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(elements.size()), GL_UNSIGNED_INT, nullptr);
}

void cleanup(GLFWwindow* window)
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

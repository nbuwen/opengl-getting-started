#ifndef OPENGLGETTINGSTARTED_JSON_H
#define OPENGLGETTINGSTARTED_JSON_H

#include <json.hpp>

nlohmann::json loadScene(const std::string& filename);

#endif //OPENGLGETTINGSTARTED_JSON_H

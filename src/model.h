#ifndef OPENGLGETTINGSTARTED_MODEL_H
#define OPENGLGETTINGSTARTED_MODEL_H


#include "mesh.h"

class Model
{
public:
    explicit Model(const std::string& path, const std::string& tpath);
    void draw() const;
	void draw(int instances) const;
private:
    std::vector<Mesh> meshes;

    void loadModel(const std::string &path, const std::string &tpath);
};


#endif //OPENGLGETTINGSTARTED_MODEL_H

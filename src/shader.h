#ifndef OPENGLGETTINGSTARTED_SHADER_H
#define OPENGLGETTINGSTARTED_SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <glad/glad.h> // include glad to get all the required OpenGL headers
#include <glm/glm.hpp>


class Shader
{
public:
    GLuint id;

    Shader(const std::string& vertex, const std::string& fragment);
    Shader(const std::string& vertex, const std::string& fragment, const std::string& tesc, const std::string& tese);

    void use() const;

    void setBool(const std::string &name, bool value) const;
    void setFloat(const std::string &name, float value) const;
    void set(const std::string& name, const glm::mat4& mat) const;
    void set(const std::string& name, const glm::vec3& vec) const;
    void set(const std::string& name, int) const;
};


#endif //OPENGLGETTINGSTARTED_SHADER_H

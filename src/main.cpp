#include <iostream>
#include <iomanip>

#include "jsonscene.h"

#include "app.h"

void help(const char* name)
{
    std::cout << "usage: " << name << " [-h] [SCENE [VALIDATION]]" << std::endl;
    std::cout << "    -h            print help message and exit" << std::endl;
    std::cout << "    SCENE         name of the scene to load, see scene folder, default is 'cube'" << std::endl;
    std::cout << "    VALIDATION    'y' or 'n' to enable validation, default is 'n'" << std::endl;
    std::cout << std::endl;
}

int main(int argc, char** argv)
{
    if(argc > 1 && (argv[1][0] == '-' && argv[1][1] == 'h'))
    {
        help(argv[0]);
        exit(0);
    }
    bool enableValidation = false;
    if(argc > 2 && argv[2][0] == 'y')
    {
        enableValidation = true;
    }
    std::string sceneName = "cube";
    if(argc > 1)
    {
        sceneName = argv[1];
    }

    auto scene = loadScene(sceneName);
    if(enableValidation)
    {
        std::clog << std::setw(2) << scene << std::endl;
    }

	App app(1920, 1080, scene);
	
	app.init();

	while (app.shouldRun())
	{	
		app.loop();
	}

	glfwTerminate();
	
	return 0;
}


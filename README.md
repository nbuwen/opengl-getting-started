Getting-Started with OpenGL
===========================

GNU/Linux Dependencies
----------------------

- `xorg-dev`

Setup
-----

- Install [CMake](https://cmake.org/)
- Have a working C++ compiler with C++14 support
- Update GPU drivers
- Clone this repository
- Run `python download.py` or download dependencies manually and generate glad loader
  - **Only do the following if you don't want to run `download.py`**
  - mkdir `external`
  - download and extract [glfw-3.2.1](http://www.glfw.org/) into `external`
  - download and extract [glm-0.9.8.x](https://glm.g-truc.net/0.9.8/index.html) into `external`
  - donwload [tinyobjloader.h](https://github.com/syoyo/tinyobjloader/blob/master/tiny_obj_loader.h) into `external`
  - donwload [stb_image.h](https://github.com/nothings/stb/blob/master/stb_image.h) into `external`
  - download [json.hpp](https://raw.githubusercontent.com/nlohmann/json/develop/single_include/nlohmann/json.hpp) into `external`
  - clone or download [glad](https://github.com/Dav1dde/glad) into `external`
    - cd into `external/glad-master`
    - run `python -m glad --profile core --generator c --extensions=, --spec gl --local-files --out-path "../glad"`
- Download assets from [here](https://www.dropbox.com/sh/fvxag57iwbo96u8/AACpCtkGCSRQmZgMXkd8MuOsa?dl=0)
  - Choose `Download -> Direct Download`
  - Extract `vulkan-assets.zip` into `opengl-getting-started`

Build
-----

- `mkdir debug` and `cd build`
- `cmake ..`
- Build with your toolchain

CLI
---

*Note: on GNU/Linux the commands must be prefixed with `./`*

- `base` runs the application with the default scene (`scenes/cube.json`)
- `base <scene-name>` runs the application with the scene `scenes/<scene-name>.json`


Hotkeys
-------

- `W A S D` move the camera forward, left, back and right
- `LSHIFT LCTRL` move the camera up and down

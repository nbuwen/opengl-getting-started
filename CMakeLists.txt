cmake_minimum_required(VERSION 3.0)
project(OpenglGettingStarted)

if(MSVC)
    # Force to always compile with W4
    if(CMAKE_CXX_FLAGS MATCHES "/W[0-4]")
        string(REGEX REPLACE "/W[0-4]" "/W4" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
    endif()
elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
    # Update if necessary
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wno-long-long -pedantic")
endif()

set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
set(GLAD_PROFILE core CACHE STRING "" FORCE)
set(GLAD_API gl=4.5 CACHE STRING "" FORCE)
set(GLAD_GENERATOR c CACHE STRING "" FORCE)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_subdirectory(external/glfw-3.2.1)
add_subdirectory(external/glad-master)
find_package(OpenGL REQUIRED)

add_executable(base src/main.cpp src/app.h src/app.cpp src/shader.cpp src/shader.h src/camera.cpp src/camera.h src/mesh.cpp src/mesh.h src/model.cpp src/model.h src/jsonscene.cpp src/jsonscene.h src/object.cpp src/object.h src/util.cpp src/util.h)

target_link_libraries(base glfw)
target_link_libraries(base glad)
target_include_directories(base PUBLIC ${OPENGL_INCLUDE_DIR})
target_link_libraries(base ${OPENGL_gl_LIBRARY})
target_include_directories(base PUBLIC external)
target_include_directories(base PUBLIC external/glm)

add_subdirectory(src/minimal)

from __future__ import print_function

from errno import EEXIST
from zipfile import ZipFile
from tarfile import open as tarfile
from os import mkdir, sep, listdir
from os.path import exists, join
from sys import stdout, path, argv


class Tarfile:
    def __init__(self, name, mode):
        self._tarfile = tarfile(name, mode)

    def namelist(self):
        return self._tarfile.getnames()

    def extractall(self, *args):
        self._tarfile.extractall(*args)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._tarfile.close()


try:
    # python 2
    import urllib2
    MODE = 'wx'
    input_ = globals()['__builtins__'].raw_input
    
    def print_(*args, **kwargs):
        flush = kwargs.pop('flush', False)
        print(*args, **kwargs)
        if flush:
            stdout.flush()
    
except ImportError:
    # python 3
    import urllib.request as urllib2
    MODE = 'bx'
    input_ = globals()['__builtins__'].input
    print_ = globals()['__builtins__'].print

PREFIX = 'external'
URLS = [
    'https://raw.githubusercontent.com/syoyo/tinyobjloader/master/tiny_obj_loader.h',
    'https://raw.githubusercontent.com/nothings/stb/master/stb_image.h',
    'https://raw.githubusercontent.com/nlohmann/json/develop/single_include/nlohmann/json.hpp',

    'https://github.com/glfw/glfw/releases/download/3.2.1/glfw-3.2.1.zip',
    'https://github.com/g-truc/glm/releases/download/0.9.8.5/glm-0.9.8.5.zip',
    'https://github.com/Dav1dde/glad/archive/master.zip'
]


def prefixed(url):
    return PREFIX + sep + url


def file_name(url):
    return prefixed(url.split("/")[-1])


def setup_folder():
    print_("creating folder '{}' ...".format(PREFIX), end="", flush=True)
    try:
        mkdir(PREFIX)
    except OSError as e:
        if e.errno == EEXIST:
            print_("\nfolder '{}' already exists, skipping".format(PREFIX))
    else:
        print_("done")
    print_()


def download_file(url):
    name = file_name(url)
    print_("downloading '{}' ...".format(name), end="", flush=True)
    try:
        with open(name, MODE) as fout:
            data = urllib2.urlopen(url).read()
            fout.write(data)
    except (OSError, IOError) as e:
        if e.errno == EEXIST:
            print_("\n'{}' already exists, skipping".format(name))
        else:
            print_("\nERROR downloading '{}': {}".format(name, e))
    else:
        print_("done")
    print_()


def extract_archive(name, archive_class):
    name = prefixed(name)
    print_("extracting '{}' ...".format(name), end="", flush=True)
    try:
        with archive_class(name, 'r') as archive:
            content_name = archive.namelist()[0]
            assert not exists(prefixed(content_name[:-1])), "'{}' already extracted, skipping".format(name)
            archive.extractall(PREFIX)
    except (OSError, IOError) as e:
        print_("\nERROR:", e)
    except AssertionError as e:
        print_("\n{}".format(e))
    else:
        print_("done")
    print_()


def generate_glad():
    module_path = join(PREFIX, 'glad-master')
    path.insert(0, module_path)

    try:
        import glad
        assert glad.__path__[0] == join(module_path, 'glad')
        from glad.__main__ import main as glad_main
    except ImportError:
        print_("Error: could not import glad module.")
        print_("Aborting")
        return
    except AssertionError:
        print_("Warning: could not import local glad module.")
        if input_("Do you want to use the global glad module instead? (y/n)").lower() != 'y':
            print_("Aborting")
            return
    else:
        argv[:] = 'glad --profile core --generator c --extensions=, --spec gl --local-files --out-path'.split()
        argv.append(join(PREFIX, 'glad'))
        glad_main()


def main():
    setup_folder()
    for url in URLS:
        download_file(url)
    for name in listdir(PREFIX):
        if name.endswith('.zip'):
            extract_archive(name, ZipFile)
        elif name.endswith('.tar.gz'):
            extract_archive(name, Tarfile)

    generate_glad()


if __name__ == '__main__':
    main()

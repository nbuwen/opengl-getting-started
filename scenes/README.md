How to create scenes
====================

Scenes are described by `json` files and can be loaded by the application with
`base <scene-name-without-".json">`. If no scene is given the default one 
(`cube.json`) is used.

E.g.: `base cube` or `base myscene`

Note that only the default `cube` scene runs without external assets.
For the other scene to work you must download the assets as described in the main readme.

Structure (WIP)
---------------

#### Scene


- Each file *must* contain exactly *one* json object (`{}`)
- The object *must* have the following key-value pairs:
  - `pipelines` a list of pipelines to create. The list *must* contain at least one pipeline object as described below
  - `objects` a list of the objects to render. The list *must* contain at least one object as described below:
- The object *can* have the following key-value pairs:
  - `lights` a list of the lights to use for the fragment shader. The list *must* either be empty or contain a small[1] number of light objects as described bellow. Default is an empty list.
  - `surface` a surface object. The value *must* be a json object of the form described later. Default is an empty object.
  - `skybox` a skybox object. The value *must* be a json object of the form described later.
  - `camera` a camera object. The value *must* be a json object of the form described later.
  
#### Surface

- The value *must* be a json object
- The object *can* contain the following key-value pairs:
  - `forceFifo` forces the swapchain to use the FIFO mode (similar to VSync) if `true`. Default is `false`.

#### Pipeline

- The value *must* be a json object
- The object *must* contain the following key-value pairs:
  - `vertexShader` the name of the vertex shader to use. The extension *must* be omitted. The name *must* correspond to one of the vertex shaders in the `shaders` folder
  - `fragmentShader` the name of the fragment shader to use. The extension *must* be omitted. The name *must* correspond to one of the fragment shaders in the `shaders` folder
- The object *can* contain the following key-value pairs:
  - `wireframe` use wireframe mode instead of polygon fill mode if `true`. Default is `false`
  - `tessellationControlShader` the name of the tessellation control shader to use. The extension *must* be omitted. The name *must* correspond to one of the tessellation control shaders in the `shaders` folder
  - `tessellationEvaluationShader` the name of the tessellation evaluation shader to use. The extension *must* be omitted. The name *must* correspond to one of the tessellation evaluation shaders in the `shaders` folder
- Either both `tessellationControlShader` *and* `tessellationEvaluationShader` *must* be specified or *none* of them. If both are specified tessellation will be enabled in the pipeline and the following key-value pairs *must* also be present
  - `patchControlPoints` the number of patch control points the tessellation stage should use. The value *must* be an integer number larger than `0` with a maximum of `maxTessellationPatchSize` (from the physical device property `limits`)

#### Skybox

- The value *must* be a json object
- The model `simple/skybox.obj` is implicitly used if a skybox is given
- The object *must* contain the following key-value pairs:
  - `left`: the left image for the cube map. The image must be present in the `textures` folder.
  - `right`: the right image for the cube map. The image must be present in the `textures` folder.
  - `top`: the top image for the cube map. The image must be present in the `textures` folder.
  - `bottom`: the bottom image for the cube map. The image must be present in the `textures` folder.
  - `front`: the front image for the cube map. The image must be present in the `textures` folder.
  - `back`: the back image for the cube map. The image must be present in the `textures` folder.
  - `pipeline` the index for the pipeline to use. The value *must* be greater or equal `0` and less than the number of pipelines specified. 
- All skybox sides *must* have the same image dimensions.

#### Object

- The value *must* be a json object
- The object *must* have the following key-value pairs:
  - `model` the name of the model file to use. The name *must* be an existing file inside the `models` folder and *must* be a valid OBJ-Wavefront file with vertex normal and texture coordinates
  - `texture` the name of the texture file to use. The name *must* be an existing file inside the `textures` folder and must contain a valid JPG, PNG, TGA, BMP, PSD, GIF, HDR or PIC file
- The object *can* have the following key-value pairs:
  - `position` the position of the object in world space. If present it *must* be an array that contains exactly three numbers. Default is `[0, 0, 0]`
  - `scale` the scale of the object in world space. If present it *must* be an array of exactly three numbers. Default is `[1, 1, 1]`
  - `rotation` the rotation of the object in world space. If present it *must* be an array of exactly three numbers. Default is `[0, 0, 0]`. Angles are in degrees
  - `instances` the number of instances to render. If present it *must* be a non-zero, positive, integer number. Default is `1`. Currently the vertex shader must handle the positioning.
  - `firstInstance` the index for the first instance to render (see `gl_InstanceIndex`). If present it *must* be a positive, integer number. Default is `0`
  - `pipeline` the index for the pipeline to use. The value *must* be greater or equal `0` and less than the number of pipelines specified. 
  - `color` the tint color which *can* be used in the shaders. If present it *must* be an array that contains exactly three numbers: `r g b` each from `0` to `1`. (Full red: `[1,0,0]`, orange: `[1,0.5,0]`)
  - `script` the name of a lua script file. If present it *must* be a string and a valid filename (without `.lua` extension) inside the `scripts` folder. See the `scripts/reame.md` for more information

#### Lights

- The value *must* be a json object
- The object *can* have the following key-value pairs:
  - `position` the position of the light in world space. If present it *must* be an array that contains exactly three numbers. Default is `[0, 0, 0]`.

#### Camera

- the value *must* be a json object
- The object *can* have the following key-value pairs:
  - `position` the start position of the camera in world space. If present it *must* be an array that contains exactly three numbers. Default is `[0, 0, -3]` (i.e. in front of the origin and looking at the origin[2]).
  - `speed` the move speed of the camera. If present it *must* be a number. Default is `4`(units per second)'

Limitations
-----------

Currently only the `texture` value of the first object (index `0`) is used. The have different textures for different objects the textures must be combined into one file.

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec3 outPosition;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec2 outTexCoord;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 color;

void main()
{
    outTexCoord = inTexCoord;
    outNormal = inNormal;
    gl_Position = model * vec4(inPosition, 1.0);
    outPosition = gl_Position.xyz;
    gl_Position = projection * view * gl_Position;
}
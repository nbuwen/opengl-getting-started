#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec3 outPosition;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec2 outTexCoord;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 color;
uniform int firstInstance;

void main()
{
    gl_Position = vec4(inPosition, 1.0);
    gl_Position = gl_Position;

    gl_Position = gl_Position;
    mat4 view_ = view * model;

    view_[0][0] = 1;
    view_[0][1] = 0;
    view_[0][2] = 0;

    view_[2][0] = 0;
    view_[2][1] = 0;
    view_[2][2] = 1;

    gl_Position = view_ * gl_Position;
    gl_Position = projection * gl_Position;

    outTexCoord = inTexCoord;
}
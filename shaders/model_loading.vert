#version 330 core
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inTexCoord;

layout (location = 0) out vec3 outNormal;
layout (location = 1) out vec2 outTexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    outTexCoord = inTexCoord;
    outNormal = inNormal;
    gl_Position = projection * view * model * vec4(inPos, 1.0);
	//gl_Position = projection * view * instanceMatrix * vec4(inPos, 1.0);

    //gl_Position = projection * view * vec4(aPos, 1.0);
}

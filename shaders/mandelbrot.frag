#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) out vec4 outColor;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;
layout(location = 3) in vec3 inTintColor;

layout(binding = 1) uniform sampler2D texSampler;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 color;

dvec2 complex_square(dvec2 z)
{
    return dvec2(z.x*z.x - z.y*z.y, 2*z.x*z.y);
}

dvec2 mandelbrot_step(dvec2 z, dvec2 c)
{
    return complex_square(z) + c;
}

bool not_element_of_set(dvec2 z)
{
    return length(z) > 4;
}

vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main()
{
    //dvec2 c = (inPosition.xy / 10000000000000.0 + dvec2(-1.75 + 0.00020006 + 0.00000000000004, 0.00004001355));
    dvec2 c = inPosition.xy;
    dvec2 z = dvec2(0, 0);

    const int MAX_STEPS = 1000;
    int steps = 0;

    for(int i = 0; i < MAX_STEPS; ++i)
    {
        z = mandelbrot_step(z, c);
        ++steps;
        if(not_element_of_set(z))
        {
            break;
        }
    }

    float factor = 1 - float(steps) / MAX_STEPS;
    float hue = pow(factor, MAX_STEPS / 20);
    vec3 rgb = hsv2rgb(vec3(hue, 1, 1));

    if(not_element_of_set(z))
    {
        outColor = vec4(rgb, 1);
    }
    else
    {
        outColor = vec4(0, 0, 0, 1);
    }
}

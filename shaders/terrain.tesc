#version 400 core
#extension GL_ARB_separate_shader_objects : enable

layout(vertices = 3) out;

layout (location = 0) in vec3 inNormal[];
layout (location = 1) in vec2 inTexCoord[];


layout (location = 0) out vec3 outNormal[3];
layout (location = 1) out vec2 outTexCoord[3];

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float time;
uniform vec3 camera;

float level(vec4 eye, vec4 point)
{
    float d = distance(eye, point);
    if(d < 10) return 64;
    if(d < 50) return 32;
    if(d < 100) return 16;
    if(d < 200) return 8;
    if(d < 400) return 4;
    if(d < 800) return 2;
    return 1;
}

void main()
{
	if(gl_InvocationID == 0)
    {
        // FROM http://in2gpu.com/2014/06/27/stitching-and-lod-using-tessellation-shaders-for-terrain-rendering/
        vec4 eye = vec4(camera, 1);

        mat4 mat = model;
        vec4 v0 = mat * gl_in[0].gl_Position / 2;
        vec4 v1 = mat * gl_in[1].gl_Position / 2;
        vec4 v2 = mat * gl_in[2].gl_Position / 2;

        vec3 d1 = v1.xyz + (v2.xyz - v1.xyz) / 2;
        vec3 d2 = v0.xyz + (v2.xyz - v0.xyz) / 2;
        vec3 d3 = v0.xyz + (v1.xyz - v0.xyz) / 2;

        float e0 = level(vec4(d1, 1), eye);
        float e1 = level(vec4(d2, 1), eye);
        float e2 = level(vec4(d3, 1), eye);
        float mine = min(e0, min(e1, e2));
        float maxe = max(e0, max(e1, e2));

        gl_TessLevelInner[0] = floor((mine + maxe) / 2);
        gl_TessLevelOuter[0] = e0;
        gl_TessLevelOuter[1] = e1;
        gl_TessLevelOuter[2] = e2;
    }
	
	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
	outTexCoord[gl_InvocationID] = inTexCoord[gl_InvocationID];
}

#version 420
#extension GL_ARB_separate_shader_objects : enable

layout(vertices = 3) out;

layout(location = 0) in vec3 ignored[];
layout(location = 1) in vec3 ignored2[];
layout(location = 2) in vec3 inNormal[];
layout(location = 3) in vec2 inTexCoord[];

layout(location = 0) out vec3 outNormal[3];
layout(location = 1) out vec2 outTexCoord[3];

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    if(gl_InvocationID == 0)
    {
        gl_TessLevelInner[0] = 64;
        gl_TessLevelOuter[0] = 64;
        gl_TessLevelOuter[1] = 64;
        gl_TessLevelOuter[2] = 64;
    }

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
    outTexCoord[gl_InvocationID] = inTexCoord[gl_InvocationID];
}

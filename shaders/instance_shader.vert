#version 330 core
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inTexCoord;

layout (location = 0) out vec3 outNormal;
layout (location = 1) out vec2 outTexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float time;

mat4 rotate(float angle, vec3 axis)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

float rand(float x, float y)
{
    return rand(vec2(x, y));
}



void main()
{

    gl_Position = vec4(inPos, 1.0);
    vec3 axis = vec3(rand(gl_InstanceID, 3.14), rand(gl_InstanceID, 1.41), rand(gl_InstanceID, 2.71));
    axis = axis * 2 - 1;
    mat4 rotation = rotate(time, axis);
    gl_Position = rotation * gl_Position;
    gl_Position = model * gl_Position;

    int x = gl_InstanceID % 10;
    int y = gl_InstanceID / 10;

    gl_Position.x += x / 30.0;
    gl_Position.z += y / 30.0;
    float d = sqrt(x*x + y*y) / 300;
    gl_Position.y += d * (rand(vec2(gl_InstanceID, 42)) * 2 - 1);

    float timeAngle = time * rand(vec2(gl_InstanceID, 1337)) / (d * 10);
    float phase = 6.2831 * rand(vec2(gl_InstanceID, 123));
    mat4 model = rotate(timeAngle + phase, vec3(0, 1, 0));

    gl_Position = projection * view * model * gl_Position;

    outTexCoord = inTexCoord;
}

#version 400 core
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inTexCoord;

layout (location = 0) out vec3 outNormal;
layout (location = 1) out vec2 outTexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float time;

const int X = 16;
const int Y = 16;

void main()
{
	gl_Position = vec4(inPos, 1.0);

    int x = gl_InstanceID % X;
    int y = gl_InstanceID / Y;

    gl_Position += vec4(x, 0, y, 0);
    gl_Position.x /= X;
    gl_Position.z /= Y;
    gl_Position = gl_Position;

    //outTexCoord = (inTexCoord + vec2(x, -y - 1)) / ivec2(X, Y);
	outTexCoord = (inTexCoord + vec2(x, y)) / 16;
}

#version 400 core
#extension GL_ARB_separate_shader_objects : enable

out vec4 FragColor;

layout (location = 0) in vec3 inNormal;
layout (location = 1) in vec2 inTexCoord;

uniform sampler2D texture_diffuse1;

void main()
{
    FragColor =  vec4(texture(texture_diffuse1, inTexCoord).rgb, 1);
}

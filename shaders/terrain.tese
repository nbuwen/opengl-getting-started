#version 400 core
#extension GL_ARB_separate_shader_objects : enable

layout(triangles, equal_spacing, cw) in;

layout (location = 0) in vec3 inNormal[];
layout (location = 1) in vec2 inTexCoord[];

layout (location = 0) out vec3 outNormal;
layout (location = 1) out vec2 outTexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float time;

uniform sampler2D texture_diffuse1;

void main()
{
	gl_Position = vec4(0, 0, 0, 1);
	outTexCoord = vec2(0, 0);

	gl_Position += gl_in[0].gl_Position * gl_TessCoord[0];
    outTexCoord += inTexCoord[0] * gl_TessCoord[0];
    gl_Position += gl_in[1].gl_Position * gl_TessCoord[1];
    outTexCoord += inTexCoord[1] * gl_TessCoord[1];
    gl_Position += gl_in[2].gl_Position * gl_TessCoord[2];
    outTexCoord += inTexCoord[2] * gl_TessCoord[2];

	gl_Position.y = texture(texture_diffuse1, outTexCoord).a;

	gl_Position = projection * view * model * gl_Position;
}

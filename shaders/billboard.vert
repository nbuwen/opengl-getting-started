#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec3 outPosition;
layout(location = 1) out vec3 outNormal;
layout(location = 2) out vec2 outTexCoord;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 color;
uniform int firstInstance;

float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec2 PLANE = vec2(1500, 1500);

void main()
{
    gl_Position = vec4(inPosition, 1.0);
    gl_Position = gl_Position;



    gl_Position = model * gl_Position;

    vec4 instanceOffset = vec4(0, 0, 0, 0);
    vec2 seed = vec2(gl_InstanceID + firstInstance, 0);
    instanceOffset.x = rand(seed) * PLANE.x - PLANE.x/2.0;
    seed.y = 1;
    instanceOffset.z = rand(seed) * PLANE.y - PLANE.y/2.0;
    instanceOffset = view * instanceOffset;

    mat4 view_ = view;

    view_[0][0] = 1;
    view_[0][1] = 0;
    view_[0][2] = 0;

    view_[2][0] = 0;
    view_[2][1] = 0;
    view_[2][2] = 1;

    gl_Position = view_ * gl_Position;
    gl_Position += instanceOffset;



    gl_Position = projection * gl_Position;

    outTexCoord = inTexCoord;
}